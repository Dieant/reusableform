import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name : 'auth',
    component: () => import('../views/AuthView.vue')
  },
  {
    path: '/product',
    name : 'product',
    component: () => import('../views/ProductsView.vue')
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
